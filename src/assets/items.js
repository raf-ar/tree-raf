import website from "../assets/website.png";
import github from "../assets/github.png";
import instagram from "../assets/instagram.png";
import secreto from "../assets/secreto.png";
import twitter from "../assets/twitter.png";
import discord from "../assets/discord.png";
import facebook from "../assets/facebook.png";

// import appstore from "../assets/playstore.png";
// import youtube from "../assets/youtube.jpeg";
// import dribbble from "../assets/dribbble.png";
// import telegram from "../assets/telegram.png";

const items = [
  {
    title: "Website",
    subtitle: "raf-ar.web.app | Personal Web",
    image: website,
    link: "https://raf-ar.web.app", //your personal website or portfolio  link
  },
  {
    title: "GitHub",
    subtitle: "@raf-ar | Personal github",
    image: github,
    link: "https://github.com/raf-ar", //Github Profile link
  },
  {
    title: "Instagram",
    subtitle: "@rafa.ar.id | Personal Instagram",
    image: instagram,
    link: "https://instagram.com/rafa.ar.id", //instagram profile link
  },
  {
    title: "Facebook",
    subtitle: "Rafa Al Razzak | Personal Facebook",
    image: facebook,
    link: "https://facebook.com/rafa.ar.id", //instagram profile link
  },
  {
    "title": "Twitter",
    "subtitle": "@rafa_ar_id",
    "image": twitter,
    "link": "https://twitter.com/rafa_ar_id"// twitter profile link
  },
  {
    title: "Discord",
    subtitle: "Raf#6856",
    image: discord,
    link: "https://discordapp.com/users/431487386120093697", //instagram profile link
  },
  {
    title: "Secreto",
    subtitle: "The Secret Messages | for someone can't talk directly",
    image: secreto,
    link: "https://s.id/s-raf-ar", //instagram profile link
  },
  // {
  //     "title": "Twitter",
  //     "subtitle": "@heysagnik | Don't forget to follow me 😉",
  //     "image": twitter,
  //     "link": "https://twitter.com/heysagnik"// twitter profile link
  // },
  // {
  //     "title": "Apps",
  //     "subtitle": "Hub of my awesome 🔥 Apps",
  //     "image": appstore,
  //     "link": "#"//
  // },
  // {
  //     "title": "YouTube",
  //     "subtitle": "@Sagnik Sahoo | Official channel of mine",
  //     "image": youtube,
  //     "link": "https://www.youtube.com/channel/UCOUa9hvd4sJWQWQRIQctbSg"//youtube channel link
  // },

  // {
  //     "title": "Dribbble",
  //     "subtitle": "@virtuonic | Hub to the Shots of my 😎 apps ",
  //     "image": dribbble,
  //     "link": "https://dribbble.com/virtuonic" // Dribbble profile link
  // },
  // {
  //     "title": "Telegram",
  //     "subtitle": "@virtuonic | Chat with me instantly, Don't hesitate! ",
  //     "image": telegram,
  //     "link": "https://telegram.me/heysagnik" //Telegram Pofile
  //}
];

export default items;
