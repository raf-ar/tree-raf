import React from 'react'

import '../css/skeleton.css'
import '../css/normalize.css'
import '../css/components.css'

function Footer() {
    return (
        <div className="Footer container">
            <p>Made with <span class="heart">❤️</span> by {"<raf />"}</p>
        </div>
    )
}

export default Footer
